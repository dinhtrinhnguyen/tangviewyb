import time
from selenium import webdriver

videoFileName = "VideoList.txt"
viewFileName = "ViewFile.txt"
btnPlaySelector = "movie_player > div.ytp-cued-thumbnail-overlay > btton"

videoFile = open(videoFileName)
listVideo = videoFile.readlines()

NUMBER_OF_TAB = 4
NUMBER_OF_VIDEO = len(listVideo)
LOOP_TIME = 3

videoIndex = 0
windowIndex = 0
tabCount = 1
viewCount = 0

#  open browser
# browser = webdriver.Chrome()
browser = webdriver.Firefox()

# open url "get(url)" tab1  windowIndex = 0
browser.get(listVideo[videoIndex])

time.sleep(2)
# click play button
playButton =browser.find_element_by_css_selector(btnPlaySelector)
playButton.click()

while True:
	videoIndex = (videoIndex + 1) % NUMBER_OF_VIDEO
	windowIndex = (windowIndex + 1) % NUMBER_OF_TAB

	if tabCount < NUMBER_OF_TAB:
		tabCount = tabCount + 1
		browser.execute_script("window.open('" + listVideo[videoIndex].strip() + "')")
		else:
			browser.switch_to.window(browser.window_handles[windowIndex])
			time.sleep(0.5)
			browser.get(listVideo[videoIndex])

		viewCount = viewCount + 1
		saveFile = open(viewFileName, "w")
		saveFile.write(str(viewCount))
		saveFile.close()

		time.sleep(LOOP_TIME)


# # open new tab windowIndex = 1
# time.sleep(0.5)
# videoIndex =videoIndex + 1
# js = "window.open('"+listVideo[videoIndex].strip()+"')"
# browser.execute_script(js)

# # go to previous tab and open new video
# time.sleep(2)
# windowIndex = 0
# handle = browser.window_handles[windowIndex]
# browser.switch_to.window()
# videoIndex = videoIndex + 1
# browser.get(listVideo[videoIndex])